package org.example.company;

import org.example.model.Person;

import java.util.Scanner;

public class Developer extends Person {
    public static Scanner sc = new Scanner(System.in);
    private String id;
    private String programLanguage;

    public Developer(){
        super();
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProgramLanguage() {
        return programLanguage;
    }

    public void setProgramLanguage(String programLanguage) {
        this.programLanguage = programLanguage;
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }

    @Override
    public int getAge() {
        return super.getAge();
    }

    @Override
    public void setAge(int age) {
        super.setAge(age);
    }

    @Override
    public void input() {
        super.input();
        System.out.print("3. ProgramLanguage: "); String programLanguage = sc.nextLine();
        System.out.print("4. Id: "); String id = sc.nextLine();
        setProgramLanguage(programLanguage);
        setId(id);
    }

    @Override
    public void displayInfo() {
        System.out.println("{Id: " +getId() + "; Tên: " + getName() + "; Tuổi: " + getAge() +"; ProgramLanguage: " + getProgramLanguage() + "}");
    }
}
