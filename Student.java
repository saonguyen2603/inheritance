package org.example.model;

public class Student extends Person{

    private double gpa;

    public Student(){
        super();
    }

    public double getGpa() {
        return gpa;
    }

    public void setGpa(double gpa) {
        this.gpa = gpa;
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }

    @Override
    public int getAge() {
        return super.getAge();
    }

    @Override
    public void setAge(int age) {
        super.setAge(age);
    }

    @Override
    public void input() {
        super.input();
        System.out.print("3.GPA: "); double gpaStudent = Double.parseDouble(sc.nextLine());
        setGpa(gpaStudent);
    }

    @Override
    public void displayInfo() {
        System.out.println("{Tên: " + getName() + "; Tuổi: " + getAge() + "; Điểm: "+ getGpa() +"}");
    }
}
