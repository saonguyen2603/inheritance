package org.example.company;

import org.example.model.Person;

import java.util.Scanner;

public class Leader extends Person {
    public static Scanner sc = new Scanner(System.in);
    public static final int BASIC_SALARY = 10000000;
    private String id;
    private int teamSize;

    public Leader() {
        super();
    }



    public int getTeamSize() {
        return teamSize;
    }

    public void setTeamSize(int teamSize) {
        this.teamSize = teamSize;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }

    @Override
    public int getAge() {
        return super.getAge();
    }

    @Override
    public void setAge(int age) {
        super.setAge(age);
    }

    @Override
    public void input() {
        super.input();
        System.out.print("3. teamSize: "); int teamSize = Integer.parseInt(sc.nextLine());
        System.out.print("4. Id: "); String id = sc.nextLine();
        setTeamSize(teamSize);
        setId(id);
    }

    @Override
    public void displayInfo() {
        System.out.println("{Id: " + getId() + "; Tên: " + getName() + "; Tuổi: " + getAge() +
                "; TeamSize: " + getTeamSize() + "; BasicSalary: " + basicSalary() +"}");
    }

    public int basicSalary(){
        int basicSalary;
        if(teamSize > 10){
            basicSalary = BASIC_SALARY * 50 / 100;
        } else {
            basicSalary = BASIC_SALARY * 10 / 100;
        }
        return basicSalary;
    }
}
