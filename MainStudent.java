package org.example.main;

import org.example.model.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MainStudent {
    public static Scanner sc = new Scanner(System.in);
    static List<Student> studentList = new ArrayList<>();
    public static void main(String[] args) {
        displayInfoStudent();
        while(true){
            System.out.println("1. Thêm học sinh.");
            System.out.println("2. Hiển thị hoc sinh.");
            System.out.print("Lựa chọn: "); int x = Integer.parseInt(sc.nextLine());
            switch (x){
                case 1:
                    System.out.print("Nhập số học sinh cần thêm: "); int num = Integer.parseInt(sc.nextLine());
                    for(int i = 0; i < num; i++){
                        System.out.println("Đang thêm học sinh số: " + (i+1));
                        Student student1 = new Student();
                        student1.input();
                        studentList.add(student1);
                    }
                    break;
                case 2:
                    displayInfoStudent();
                    break;
                default:
                    System.out.println("Nhập sai vui lòng nhập lai");
            }
        }
    }
    public static void displayInfoStudent(){
        for(Student a: studentList){
            a.displayInfo();
        }
    }
}
