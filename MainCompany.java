package org.example.main;

import org.example.company.Developer;
import org.example.company.Leader;
import org.example.company.Tester;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MainCompany {
    public static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        while (true) {
            System.out.println("1. Nhập thông tin của Developer.");
            System.out.println("2. Nhập thông tin của Tester.");
            System.out.println("3. Nhập thông tin của Leader.");
            System.out.println("4. Hiển thị thông tin Developer");
            System.out.println("5. Hiển thị thông tin Tester");
            System.out.println("6. Hiển thị thông tin Leader");
            System.out.println("7. Hiển thị thông tin lập trình viên Java.");
            System.out.println("8. Hiển thị Leader có người trong đội lớn hơn 10.");
            System.out.print(" Bạn chọn: ");
            int x = Integer.parseInt(sc.nextLine());
            switch (x) {
                case 1:
                    System.out.print("Số lượng Dev được nhập: ");
                    InputDev();
                    break;
                case 2:
                    System.out.print("Số lượng Tester được nhập: ");
                    InputTest();
                    break;
                case 3:
                    System.out.print("Số lượng Leader được nhập: ");
                    InputLead();
                    break;
                case 4:
                    System.out.println("Thông tin Dev hiện tại");
                    showInfoDev();
                    break;
                case 5:
                    System.out.println("Thông tin Tester hiện tại");
                    showInfoTester();
                    break;
                case 6:
                    System.out.println("Thông tin Leader hiện tại");
                    showInfoLead();
                    break;
                case 7:
                    System.out.println("Thông tin lập trình viên java");
                    findProgramLanguage();
                    break;
                case 8:
                    System.out.println("Thông tin leader có người trong đội lớn hơn 10.");
                    findLeader();
                    break;
                default:
                    System.out.println("Nhập chưa đúng hãy nhập lại.");

            }
        }
    }

    static List<Developer> developerList = new ArrayList<>();
    static List<Tester> testerList = new ArrayList<>();
    static List<Leader> leaderList = new ArrayList<>();

    public static void InputDev() {
        int num = Integer.parseInt(sc.nextLine());
        for (int i = 0; i < num; i++) {
            System.out.println("Đang thêm Dev số: " + (i+1));
            Developer developer = new Developer();
            developer.input();
            developerList.add(developer);
        }
    }

    public static void InputTest() {
        int num = Integer.parseInt(sc.nextLine());
        for (int i = 0; i < num; i++) {
            System.out.println("Đang thêm Tester số: " + (i+1));
            Tester tester = new Tester();
            tester.input();
            testerList.add(tester);
        }
    }

    public static void InputLead() {
        int num = Integer.parseInt(sc.nextLine());
        for (int i = 0; i < num; i++) {
            System.out.println("Đang thêm Leader số: " + (i+1));
            Leader leader = new Leader();
            leader.input();
            leaderList.add(leader);
        }
    }

    public static void showInfoDev() {
        for (Developer a : developerList) {
            a.displayInfo();
        }
    }

    public static void showInfoTester() {
        for (Tester a : testerList) {
            a.displayInfo();
        }
    }

    public static void showInfoLead() {
        for (Leader a : leaderList) {
            a.displayInfo();
        }
    }

    public static void findProgramLanguage() {
        for (Developer a : developerList) {
            if (a.getProgramLanguage().equalsIgnoreCase("java")) {
                a.displayInfo();
            }
        }
    }

    public static void findLeader(){
        for(Leader a :leaderList){
            if(a.getTeamSize() > 10){
                a.displayInfo();
            }
        }
    }
}
