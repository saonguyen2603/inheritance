package org.example.main;

import org.example.model.Teacher;

import java.util.*;

public class MainTeacher {
    public static Scanner sc = new Scanner(System.in);
    public static List<Teacher> teacherList = new ArrayList<>();

    public static void main(String[] args) {
        displayInfoTeacher();

        while (true) {
            System.out.println("1. Thêm giáo viên.");
            System.out.println("2. Hiển thị giáo viên.");
            System.out.print("Lựa chọn: ");
            int x = Integer.parseInt(sc.nextLine());
            switch (x) {
                case 1:
                    System.out.print("số giáo viên cần thêm: "); int num = Integer.parseInt(sc.nextLine());
                    for(int i = 0; i < num; i++){
                        System.out.println("Đang thêm giáo viên số: " + (i+1));
                        Teacher teacher = new Teacher();
                        teacher.input();
                        teacherList.add(teacher);
                    }
                    break;
                case 2:
                    displayInfoTeacher();
                    break;
                default:
                    System.out.println("Nhập sai vui lòng nhập lại.");
            }
        }
    }

    public static void displayInfoTeacher() {
        for (Teacher a : teacherList) {
            a.displayInfo();
        }
    }
}
