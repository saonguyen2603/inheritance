package org.example.model;

import java.util.Scanner;

public class Person {

    Scanner sc = new Scanner(System.in);

    protected String name;
    protected int age;
    protected Person(){

    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void input(){
        System.out.print("1. Tên: "); String name1 = sc.nextLine();
        System.out.print("2. Tuổi: "); int age1 = Integer.parseInt(sc.nextLine());
        setName(name1);
        setAge(age1);
    }

    public void displayInfo(){
        System.out.println("{Tên: " + getName() + "; Tuổi: " + getAge() + "}");
    }
}
